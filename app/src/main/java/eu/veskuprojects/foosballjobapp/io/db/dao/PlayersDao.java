package eu.veskuprojects.foosballjobapp.io.db.dao;

import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import eu.veskuprojects.foosballjobapp.utils.DBConstants;

public interface PlayersDao {

    @Insert
    long addNewPlayer();

    @Query("DELETE FROM " + DBConstants.TABLE_PLAYERS + " WHERE " + DBConstants.COLUMN_PLAYER_ID + " = :playerId")
    void removePlayer(int playerId);

    @Query("UPDATE " + DBConstants.TABLE_PLAYERS + " SET " + DBConstants.COLUMN_PLAYER_NAME + " = :playerName WHERE " + DBConstants.COLUMN_PLAYER_ID + " = :playerId")
    void updatePlayerName(int playerId, int playerName);

}
