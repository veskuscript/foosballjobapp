package eu.veskuprojects.foosballjobapp.io.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import eu.veskuprojects.foosballjobapp.io.db.model.GameEntity;
import eu.veskuprojects.foosballjobapp.utils.DBConstants;

@Database(entities = {GameEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static volatile AppDatabase databaseInstance;

    public static synchronized AppDatabase getDatabaseInstance(Context context) {
        if(databaseInstance == null) {
            databaseInstance = createDatabase(context);
        }

        return databaseInstance;
    }

    private static AppDatabase createDatabase(final Context context) {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                DBConstants.DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }
}
