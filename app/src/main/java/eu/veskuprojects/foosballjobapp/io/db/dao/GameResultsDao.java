package eu.veskuprojects.foosballjobapp.io.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import eu.veskuprojects.foosballjobapp.io.db.model.GameEntity;
import eu.veskuprojects.foosballjobapp.io.db.model.GamePlayerEntity;
import eu.veskuprojects.foosballjobapp.utils.DBConstants;

@Dao
public interface GameResultsDao {

    @Insert
    void addNewGame(GameEntity gameEntity);

    @Query("DELETE FROM " + DBConstants.TABLE_GAME_RESULTS + " WHERE " + DBConstants.COLUMN_GAME_ID + " = :gameId")
    void removeGame(int gameId);

    @Query("UPDATE " + DBConstants.TABLE_GAME_RESULTS +
            " SET " + DBConstants.COLUMN_FIRST_PLAYER_SCORE + " = :firstPlayerScore, " +
            DBConstants.COLUMN_SECOND_PLAYER_SCORE + " = :secondPlayerScore WHERE " + DBConstants.COLUMN_GAME_ID + " = :gameId")
    void updateGameScore(int gameId, int firstPlayerScore, int secondPlayerScore);

    @Query("SELECT " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_FIRST_PLAYER_ID + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_SECOND_PLAYER_ID + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_FIRST_PLAYER_SCORE + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_SECOND_PLAYER_SCORE + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_DATE_OF_GAME + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_WINNER_PLAYER_ID +
            " FROM " + DBConstants.TABLE_GAME_RESULTS +
            " INNER JOIN " + DBConstants.TABLE_PLAYERS + " AS " + DBConstants.COLUMN_PLAYER_NAME +
            " ON (" + DBConstants.TABLE_PLAYERS + "." + DBConstants.COLUMN_PLAYER_ID + " = " + DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_FIRST_PLAYER_ID + ") " +
            " INNER JOIN " + DBConstants.TABLE_PLAYERS + " AS " + DBConstants.COLUMN_PLAYER_NAME +
            " ON (" + DBConstants.TABLE_PLAYERS + "." + DBConstants.COLUMN_PLAYER_ID + " = " + DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_SECOND_PLAYER_ID + ") " +
            " ORDER by " + DBConstants.TABLE_PLAYERS + "." + DBConstants.COLUMN_DATE_OF_GAME + " DESC")
    List<GamePlayerEntity> getGameResultsSortedByCount();

    @Query("SELECT " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_FIRST_PLAYER_ID + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_SECOND_PLAYER_ID + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_FIRST_PLAYER_SCORE + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_SECOND_PLAYER_SCORE + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_DATE_OF_GAME + ", " +
            DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_WINNER_PLAYER_ID +
            " FROM " + DBConstants.TABLE_GAME_RESULTS +
            " INNER JOIN " + DBConstants.TABLE_PLAYERS + " AS " + DBConstants.COLUMN_PLAYER_NAME +
            " ON (" + DBConstants.TABLE_PLAYERS + "." + DBConstants.COLUMN_PLAYER_ID + " = " + DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_FIRST_PLAYER_ID + ") " +
            " INNER JOIN " + DBConstants.TABLE_PLAYERS + " AS " + DBConstants.COLUMN_PLAYER_NAME +
            " ON (" + DBConstants.TABLE_PLAYERS + "." + DBConstants.COLUMN_PLAYER_ID + " = " + DBConstants.TABLE_GAME_RESULTS + "." + DBConstants.COLUMN_SECOND_PLAYER_ID + ") " +
            " ORDER by " + DBConstants.TABLE_PLAYERS + "." + DBConstants.COLUMN_DATE_OF_GAME + " DESC")
    List<GamePlayerEntity> getGameResultsSortedByWins();
}
