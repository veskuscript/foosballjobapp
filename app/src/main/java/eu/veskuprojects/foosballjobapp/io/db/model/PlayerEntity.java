package eu.veskuprojects.foosballjobapp.io.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

import eu.veskuprojects.foosballjobapp.utils.DBConstants;

@Entity(tableName = DBConstants.TABLE_PLAYERS)
public class PlayerEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBConstants.COLUMN_PLAYER_ID)
    int playerId;

    @ColumnInfo(name = DBConstants.COLUMN_PLAYER_NAME)
    String playerName;

    public PlayerEntity(int playerId, String playerName) {
        this.playerId = playerId;
        this.playerName = playerName;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
