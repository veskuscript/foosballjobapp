package eu.veskuprojects.foosballjobapp.io.db.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import java.io.Serializable;

import eu.veskuprojects.foosballjobapp.utils.DBConstants;

@Entity(tableName = DBConstants.TABLE_GAME_RESULTS)
public class GameEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = DBConstants.COLUMN_GAME_ID)
    int gameId;

    @ColumnInfo(name = DBConstants.COLUMN_FIRST_PLAYER_ID)
    int firstPlayerId;

    @ColumnInfo(name = DBConstants.COLUMN_SECOND_PLAYER_ID)
    int secondPlayerId;

    @ColumnInfo(name = DBConstants.COLUMN_WINNER_PLAYER_ID)
    int winnerPlayerId;

    @ColumnInfo(name = DBConstants.COLUMN_DATE_OF_GAME)
    long dateOfGame;

    @ColumnInfo(name = DBConstants.COLUMN_FIRST_PLAYER_SCORE)
    int firstPlayerScore;

    @ColumnInfo(name = DBConstants.COLUMN_SECOND_PLAYER_SCORE)
    int secondPlayerScore;

    public GameEntity(int gameId, int firstPlayerId, int secondPlayerId, int winnerPlayerId, long dateOfGame, int firstPlayerScore, int secondPlayerScore) {
        this.gameId = gameId;
        this.firstPlayerId = firstPlayerId;
        this.secondPlayerId = secondPlayerId;
        this.winnerPlayerId = winnerPlayerId;
        this.dateOfGame = dateOfGame;
        this.firstPlayerScore = firstPlayerScore;
        this.secondPlayerScore = secondPlayerScore;
    }

    public int getGameId() {
        return gameId;
    }

    public int getFirstPlayerId() {
        return firstPlayerId;
    }

    public void setFirstPlayerId(int firstPlayerId) {
        this.firstPlayerId = firstPlayerId;
    }

    public int getSecondPlayerId() {
        return secondPlayerId;
    }

    public void setSecondPlayerId(int secondPlayerId) {
        this.secondPlayerId = secondPlayerId;
    }

    public int getWinnerPlayerId() {
        return winnerPlayerId;
    }

    public void setWinnerPlayerId(int winnerPlayerId) {
        this.winnerPlayerId = winnerPlayerId;
    }

    public long getDateOfGame() {
        return dateOfGame;
    }

    public void setDateOfGame(long dateOfGame) {
        this.dateOfGame = dateOfGame;
    }

    public int getFirstPlayerScore() {
        return firstPlayerScore;
    }

    public void setFirstPlayerScore(int firstPlayerScore) {
        this.firstPlayerScore = firstPlayerScore;
    }

    public int getSecondPlayerScore() {
        return secondPlayerScore;
    }

    public void setSecondPlayerScore(int secondPlayerScore) {
        this.secondPlayerScore = secondPlayerScore;
    }
}
