package eu.veskuprojects.foosballjobapp.utils;

public class DBConstants {

    //DB file name
    public static final String DB_NAME = "results.db";

    //Table names
    public static final String TABLE_GAME_RESULTS =                 "games";
    public static final String TABLE_PLAYERS =                      "players";

    //Column names
    public static final String COLUMN_GAME_ID =                     "game_id";
    public static final String COLUMN_PLAYER_ID =                   "player_id";
    public static final String COLUMN_PLAYER_NAME =                 "player_name";
    public static final String COLUMN_FIRST_PLAYER_ID =             "first_player_name";
    public static final String COLUMN_SECOND_PLAYER_ID =            "second_player_name";
    public static final String COLUMN_WINNER_PLAYER_ID =            "winner_player_id";
    public static final String COLUMN_FIRST_PLAYER_SCORE =          "first_player_score";
    public static final String COLUMN_SECOND_PLAYER_SCORE =         "second_player_score";
    public static final String COLUMN_DATE_OF_GAME =                "date_of_game";
}
