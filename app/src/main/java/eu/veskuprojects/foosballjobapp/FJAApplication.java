package eu.veskuprojects.foosballjobapp;

import android.app.Application;

import eu.veskuprojects.foosballjobapp.dagger.Injector;

public class FJAApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Injector.initAppComponent(this);
    }
}
