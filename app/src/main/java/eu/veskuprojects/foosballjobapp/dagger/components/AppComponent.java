package eu.veskuprojects.foosballjobapp.dagger.components;

import javax.inject.Singleton;

import dagger.Component;
import eu.veskuprojects.foosballjobapp.dagger.modules.AppModule;

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
}
