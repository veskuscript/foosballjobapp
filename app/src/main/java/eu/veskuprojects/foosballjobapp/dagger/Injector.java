package eu.veskuprojects.foosballjobapp.dagger;

import android.app.Application;

import eu.veskuprojects.foosballjobapp.dagger.components.AppComponent;
import eu.veskuprojects.foosballjobapp.dagger.components.DaggerAppComponent;
import eu.veskuprojects.foosballjobapp.dagger.modules.AppModule;

public class Injector {

    private static AppComponent appComponent;

    public static void initAppComponent(Application application) {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
