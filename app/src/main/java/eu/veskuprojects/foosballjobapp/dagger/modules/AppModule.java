package eu.veskuprojects.foosballjobapp.dagger.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eu.veskuprojects.foosballjobapp.io.db.AppDatabase;

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    private AppDatabase provideAppDatabase() {
        return AppDatabase.getDatabaseInstance(application);
    }
}
