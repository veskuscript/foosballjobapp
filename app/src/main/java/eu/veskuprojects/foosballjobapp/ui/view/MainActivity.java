package eu.veskuprojects.foosballjobapp.ui.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import eu.veskuprojects.foosballjobapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
